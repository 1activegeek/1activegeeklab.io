Some reference info/links

[GitLab Pages Documentation](https://docs.gitlab.com/ee/user/project/pages/index.html#getting-started-with-gitlab-pages)

[Getting Started with GitLab CI](https://docs.gitlab.com/ce/ci/quick_start/)

[HTML5 UP Themes](https://html5up.net)

[Minimal Mistakes Config Docs](https://mmistakes.github.io/minimal-mistakes/docs/configuration/)

